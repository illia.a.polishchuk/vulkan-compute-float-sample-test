#include <ctype.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vulkan/vulkan.h>

uint32_t g_float_bad_value = 0xffffffff;

/*
#version 430 core

layout(binding = 0) uniform sampler2D texSampler;

layout(std430, binding = 1) buffer g_shader_out_data
{
    float outData;
};

void main()
{
    outData = texture(texSampler, vec2(0.0, 0.0)).x;
}
*/
unsigned char shader_compute_spv[820] = {
    0x03, 0x02, 0x23, 0x07, 0x00, 0x00, 0x01, 0x00, 0x0a, 0x00, 0x0d, 0x00, 0x1b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x11, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x47, 0x4c,
    0x53, 0x4c, 0x2e, 0x73, 0x74, 0x64, 0x2e, 0x34, 0x35, 0x30, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x03, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x05, 0x00, 0x05, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
    0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x06, 0x00, 0x04, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00,
    0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x02, 0x00,
    0x00, 0x00, 0xae, 0x01, 0x00, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x47, 0x4c, 0x5f, 0x47, 0x4f, 0x4f, 0x47, 0x4c, 0x45,
    0x5f, 0x63, 0x70, 0x70, 0x5f, 0x73, 0x74, 0x79, 0x6c, 0x65, 0x5f, 0x6c, 0x69, 0x6e, 0x65, 0x5f, 0x64, 0x69, 0x72,
    0x65, 0x63, 0x74, 0x69, 0x76, 0x65, 0x00, 0x00, 0x04, 0x00, 0x08, 0x00, 0x47, 0x4c, 0x5f, 0x47, 0x4f, 0x4f, 0x47,
    0x4c, 0x45, 0x5f, 0x69, 0x6e, 0x63, 0x6c, 0x75, 0x64, 0x65, 0x5f, 0x64, 0x69, 0x72, 0x65, 0x63, 0x74, 0x69, 0x76,
    0x65, 0x00, 0x05, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x05,
    0x00, 0x07, 0x00, 0x07, 0x00, 0x00, 0x00, 0x67, 0x5f, 0x73, 0x68, 0x61, 0x64, 0x65, 0x72, 0x5f, 0x6f, 0x75, 0x74,
    0x5f, 0x64, 0x61, 0x74, 0x61, 0x00, 0x00, 0x00, 0x06, 0x00, 0x05, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x6f, 0x75, 0x74, 0x44, 0x61, 0x74, 0x61, 0x00, 0x05, 0x00, 0x03, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x05, 0x00, 0x05, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x74, 0x65, 0x78, 0x53, 0x61, 0x6d, 0x70, 0x6c, 0x65,
    0x72, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x03, 0x00, 0x07, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04,
    0x00, 0x09, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x09, 0x00,
    0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x22,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x13, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x21, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x16, 0x00, 0x03, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x1e, 0x00,
    0x03, 0x00, 0x07, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x02,
    0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00,
    0x02, 0x00, 0x00, 0x00, 0x15, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
    0x00, 0x2b, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x19, 0x00,
    0x09, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1b, 0x00, 0x03, 0x00,
    0x0d, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x0d, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x17, 0x00, 0x04, 0x00, 0x11, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x2b,
    0x00, 0x04, 0x00, 0x06, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2c, 0x00, 0x05, 0x00,
    0x11, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x17, 0x00, 0x04,
    0x00, 0x14, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x15, 0x00, 0x04, 0x00, 0x16, 0x00,
    0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04, 0x00, 0x16, 0x00, 0x00, 0x00, 0x17,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x19, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
    0x06, 0x00, 0x00, 0x00, 0x36, 0x00, 0x05, 0x00, 0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x03, 0x00, 0x00, 0x00, 0xf8, 0x00, 0x02, 0x00, 0x05, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00, 0x0d, 0x00,
    0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x58, 0x00, 0x07, 0x00, 0x14, 0x00, 0x00, 0x00, 0x15,
    0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00,
    0x51, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x41, 0x00, 0x05, 0x00, 0x19, 0x00, 0x00, 0x00, 0x1a, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x0b, 0x00,
    0x00, 0x00, 0x3e, 0x00, 0x03, 0x00, 0x1a, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0xfd, 0x00, 0x01, 0x00, 0x38,
    0x00, 0x01, 0x00};

#define ERROR_CHECK(ERROR_CODE, ERROR_STRING)                                                                          \
    if (ERROR_CODE)                                                                                                    \
    {                                                                                                                  \
        fprintf(stderr, "ERROR: line: %d ; code: %d ; message: %s\n", __LINE__, ERROR_CODE, ERROR_STRING);             \
        abort();                                                                                                       \
    }

void copyBufferToImage(VkDevice logicalDevice, VkCommandPool commandPool, VkQueue computeQueue, VkBuffer buffer,
                       VkImage image, uint32_t width, uint32_t height)
{
    VkCommandBufferAllocateInfo allocInfo = {0};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
    VkResult result = vkAllocateCommandBuffers(logicalDevice, &allocInfo, &commandBuffer);
    ERROR_CHECK(result, "transitionImageLayout vkAllocateCommandBuffers failed");

    VkCommandBufferBeginInfo beginInfo = {0};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);
    ERROR_CHECK(result, "transitionImageLayout vkBeginCommandBuffer failed");

    VkBufferImageCopy region = {0};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageExtent.width = width;
    region.imageExtent.height = height;
    region.imageExtent.depth = 1;

    vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    result = vkEndCommandBuffer(commandBuffer);
    ERROR_CHECK(result, "transitionImageLayout vkEndCommandBuffer failed");

    VkSubmitInfo submitInfo = {0};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    result = vkQueueSubmit(computeQueue, 1, &submitInfo, VK_NULL_HANDLE);
    ERROR_CHECK(result, "transitionImageLayout vkQueueSubmit failed");
    result = vkQueueWaitIdle(computeQueue);
    ERROR_CHECK(result, "transitionImageLayout vkQueueSubmit failed");

    // cleanup
    if (commandBuffer)
        vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);
}

void transitionImageLayout(VkDevice logicalDevice, VkCommandPool commandPool, VkQueue computeQueue, VkImage image,
                           VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
{
    VkCommandBufferAllocateInfo allocInfo = {0};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
    VkResult result = vkAllocateCommandBuffers(logicalDevice, &allocInfo, &commandBuffer);
    ERROR_CHECK(result, "transitionImageLayout vkAllocateCommandBuffers failed");

    VkCommandBufferBeginInfo beginInfo = {0};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);
    ERROR_CHECK(result, "transitionImageLayout vkBeginCommandBuffer failed");

    VkImageMemoryBarrier barrier = {0};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
    }

    vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, NULL, 0, NULL, 1, &barrier);

    result = vkEndCommandBuffer(commandBuffer);
    ERROR_CHECK(result, "transitionImageLayout vkEndCommandBuffer failed");

    VkSubmitInfo submitInfo = {0};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    result = vkQueueSubmit(computeQueue, 1, &submitInfo, VK_NULL_HANDLE);
    ERROR_CHECK(result, "transitionImageLayout vkQueueSubmit failed");
    result = vkQueueWaitIdle(computeQueue);
    ERROR_CHECK(result, "transitionImageLayout vkQueueSubmit failed");

    // cleanup
    if (commandBuffer)
        vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);
}

void createFloat1x1TextureImage(VkDevice logicalDevice, VkPhysicalDeviceMemoryProperties memProperties,
                                VkCommandPool commandPool, VkQueue computeQueue, VkImage *out_textureImage,
                                VkDeviceMemory *out_textureMemory)
{
    VkBufferCreateInfo textureBufferCreateInfo = {0};
    textureBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    textureBufferCreateInfo.size = sizeof(float);
    textureBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    textureBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkBuffer textureBuffer = VK_NULL_HANDLE;
    VkResult result = vkCreateBuffer(logicalDevice, &textureBufferCreateInfo, NULL, &textureBuffer);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkCreateDevice failed");

    VkMemoryRequirements memRequirementsTextureBuffer = {0};
    vkGetBufferMemoryRequirements(logicalDevice, textureBuffer, &memRequirementsTextureBuffer);

    uint32_t textureBufferIndex = 0;
    int textureBufferIndexFound = VK_FALSE;
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
    {
        if ((memRequirementsTextureBuffer.memoryTypeBits & (1 << i)) &&
            memProperties.memoryTypes[i].propertyFlags &
                (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
        {
            textureBufferIndex = i;
            textureBufferIndexFound = VK_TRUE;
            break;
        }
    }
    ERROR_CHECK(!textureBufferIndexFound, "createFloat1x1TextureImage textureBufferIndexFound failed");

    VkMemoryAllocateInfo allocInfoTextureBuffer = {0};
    allocInfoTextureBuffer.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfoTextureBuffer.allocationSize = memRequirementsTextureBuffer.size;
    allocInfoTextureBuffer.memoryTypeIndex = textureBufferIndex;

    VkDeviceMemory textureBufferMemory = VK_NULL_HANDLE;
    result = vkAllocateMemory(logicalDevice, &allocInfoTextureBuffer, NULL, &textureBufferMemory);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkAllocateMemory failed");

    result = vkBindBufferMemory(logicalDevice, textureBuffer, textureBufferMemory, 0);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkBindBufferMemory failed");

    float *textureBufferMapped = NULL;
    result = vkMapMemory(logicalDevice, textureBufferMemory, 0, textureBufferCreateInfo.size, 0,
                         (void **)&textureBufferMapped);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkMapMemory failed");

    // set texture value
    uint32_t float_nan_value = UINT32_MAX;
    *textureBufferMapped = *((float *)&g_float_bad_value);

    VkImageCreateInfo imageInfo = {0};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = 1;
    imageInfo.extent.height = 1;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = VK_FORMAT_R32_SFLOAT;
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    result = vkCreateImage(logicalDevice, &imageInfo, NULL, out_textureImage);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkCreateImage failed");

    VkMemoryRequirements memRequirementsImage = {0};
    vkGetImageMemoryRequirements(logicalDevice, *out_textureImage, &memRequirementsImage);

    uint32_t textureIndex = 0;
    int textureIndexFound = VK_FALSE;
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
    {
        if ((memRequirementsImage.memoryTypeBits & (1 << i)) &&
            memProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            textureIndex = i;
            textureIndexFound = VK_TRUE;
            break;
        }
    }
    ERROR_CHECK(!textureIndexFound, "createFloat1x1TextureImage textureIndexFound failed");

    VkMemoryAllocateInfo allocInfoTexture = {0};
    allocInfoTexture.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfoTexture.allocationSize = memRequirementsImage.size;
    allocInfoTexture.memoryTypeIndex = textureIndex;

    result = vkAllocateMemory(logicalDevice, &allocInfoTexture, NULL, out_textureMemory);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkAllocateMemory failed");

    result = vkBindImageMemory(logicalDevice, *out_textureImage, *out_textureMemory, 0);
    ERROR_CHECK(result, "createFloat1x1TextureImage vkBindImageMemory failed");

    transitionImageLayout(logicalDevice, commandPool, computeQueue, *out_textureImage, VK_FORMAT_R32_SFLOAT,
                          VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    copyBufferToImage(logicalDevice, commandPool, computeQueue, textureBuffer, *out_textureImage, 1, 1);

    transitionImageLayout(logicalDevice, commandPool, computeQueue, *out_textureImage, VK_FORMAT_R32_SFLOAT,
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // cleanup
    if (textureBufferMemory)
        vkFreeMemory(logicalDevice, textureBufferMemory, NULL);

    if (textureBuffer)
        vkDestroyBuffer(logicalDevice, textureBuffer, NULL);
}

int main()
{
    VkApplicationInfo appInfo = {0};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "vulkan-compute-minimal-sample";
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instanceCreateInfo = {0};
    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pApplicationInfo = &appInfo;

#ifdef __APPLE__ // Enable extensions for MoltenVK support
    const char *portabilityExtension = VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME;
    instanceCreateInfo.ppEnabledExtensionNames = &portabilityExtension;
    instanceCreateInfo.enabledExtensionCount = 1;
    instanceCreateInfo.flags |= VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR;
#endif

    VkInstance instance = VK_NULL_HANDLE;
    VkResult result = vkCreateInstance(&instanceCreateInfo, NULL, &instance);
    ERROR_CHECK(result, "vkCreateInstance failed");

    uint32_t physicalDeviceCount = 0;
    result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, NULL);
    ERROR_CHECK(result, "vkEnumeratePhysicalDevices failed");
    ERROR_CHECK(!physicalDeviceCount, "vkEnumeratePhysicalDevices physical devices not found");

    VkPhysicalDevice *physicalDevices = malloc(sizeof(VkPhysicalDevice) * physicalDeviceCount);
    ERROR_CHECK(!physicalDevices, "VkPhysicalDevice malloc failed");

    result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDevices);
    ERROR_CHECK(result, "vkEnumeratePhysicalDevices failed");

    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

    // 1 priority -- Intel
    for (uint32_t i = 0; i < physicalDeviceCount; ++i)
    {
        VkPhysicalDeviceProperties deviceProperties = {0};
        vkGetPhysicalDeviceProperties(physicalDevices[i], &deviceProperties);

        char deviceNameLower[VK_MAX_PHYSICAL_DEVICE_NAME_SIZE] = {0};
        for (size_t i = 0; deviceProperties.deviceName[i]; ++i)
            deviceNameLower[i] = tolower(deviceProperties.deviceName[i]);

        if (strstr(deviceNameLower, "intel") != NULL)
        {
            physicalDevice = physicalDevices[i];
            break;
        }
    }

    // 2 priority -- First in order
    if (!physicalDevice)
    {
        physicalDevice = physicalDevices[0];
    }

    VkPhysicalDeviceProperties deviceProperties = {0};
    vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

    printf("INFO: Selected physical device: %s\n\n", deviceProperties.deviceName);

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, NULL);

    VkQueueFamilyProperties *queueFamilies = malloc(sizeof(VkQueueFamilyProperties) * queueFamilyCount);
    ERROR_CHECK(!queueFamilies, "VkQueueFamilyProperties malloc failed");

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies);

    uint32_t computeQueueFamilyIndex = -1;
    int computeQueueFamilyFound = VK_FALSE;
    for (uint32_t i = 0; i < queueFamilyCount; ++i)
    {
        if (queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            computeQueueFamilyIndex = i;
            computeQueueFamilyFound = VK_TRUE;
            break;
        }
    }

    ERROR_CHECK(!computeQueueFamilyFound, "vkGetPhysicalDeviceQueueFamilyProperties queue family with "
                                          "compute bit not found");

    VkDeviceQueueCreateInfo queueCreateInfo = {0};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = computeQueueFamilyIndex;
    queueCreateInfo.queueCount = 1;
    float queuePriority = 1.0f;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures deviceFeatures = {0};

    VkDeviceCreateInfo deviceCreateInfo = {0};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

    VkDevice logicalDevice = VK_NULL_HANDLE;
    result = vkCreateDevice(physicalDevice, &deviceCreateInfo, NULL, &logicalDevice);
    ERROR_CHECK(result, "vkCreateDevice failed");

    VkQueue computeQueue = VK_NULL_HANDLE;
    vkGetDeviceQueue(logicalDevice, computeQueueFamilyIndex, 0, &computeQueue);
    ERROR_CHECK(!computeQueue, "vkGetDeviceQueue failed");

    VkBufferCreateInfo bufferCreateInfo = {0};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = sizeof(float);
    bufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkPhysicalDeviceMemoryProperties memProperties = {0};
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

    VkCommandPoolCreateInfo commandPoolInfo = {0};
    commandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    commandPoolInfo.queueFamilyIndex = computeQueueFamilyIndex;

    VkCommandPool commandPool = VK_NULL_HANDLE;
    result = vkCreateCommandPool(logicalDevice, &commandPoolInfo, NULL, &commandPool);
    ERROR_CHECK(result, "vkCreateCommandPool failed");

    VkImage textureImage = VK_NULL_HANDLE;
    VkDeviceMemory textureMemory = VK_NULL_HANDLE;
    createFloat1x1TextureImage(logicalDevice, memProperties, commandPool, computeQueue, &textureImage, &textureMemory);

    VkImageViewCreateInfo imageViewInfo = {0};
    imageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewInfo.image = textureImage;
    imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imageViewInfo.format = VK_FORMAT_R32_SFLOAT;
    imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageViewInfo.subresourceRange.baseMipLevel = 0;
    imageViewInfo.subresourceRange.levelCount = 1;
    imageViewInfo.subresourceRange.baseArrayLayer = 0;
    imageViewInfo.subresourceRange.layerCount = 1;

    VkImageView textureImageView = VK_NULL_HANDLE;
    result = vkCreateImageView(logicalDevice, &imageViewInfo, NULL, &textureImageView);
    ERROR_CHECK(result, "vkCreateImageView failed");

    VkSamplerCreateInfo samplerInfo = {0};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_NEAREST;
    samplerInfo.minFilter = VK_FILTER_NEAREST;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_FALSE;
    samplerInfo.maxAnisotropy = deviceProperties.limits.maxSamplerAnisotropy;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

    VkSampler textureSampler = VK_NULL_HANDLE;
    result = vkCreateSampler(logicalDevice, &samplerInfo, NULL, &textureSampler);
    ERROR_CHECK(result, "vkCreateSampler failed");

    VkBuffer outBuffer = VK_NULL_HANDLE;
    result = vkCreateBuffer(logicalDevice, &bufferCreateInfo, NULL, &outBuffer);
    ERROR_CHECK(result, "vkCreateDevice failed");

    VkMemoryRequirements memRequirementsOut = {0};
    vkGetBufferMemoryRequirements(logicalDevice, outBuffer, &memRequirementsOut);

    uint32_t memoryIndexOut = 0;
    int memoryIndexOutFound = VK_FALSE;
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
    {
        if ((memRequirementsOut.memoryTypeBits & (1 << i)) &&
            memProperties.memoryTypes[i].propertyFlags &
                (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
        {
            memoryIndexOut = i;
            memoryIndexOutFound = VK_TRUE;
            break;
        }
    }
    ERROR_CHECK(!memoryIndexOutFound, "memoryIndexOutFound failed");

    VkMemoryAllocateInfo allocInfoOut = {0};
    allocInfoOut.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfoOut.allocationSize = memRequirementsOut.size;
    allocInfoOut.memoryTypeIndex = memoryIndexOut;

    VkDeviceMemory outMemory = VK_NULL_HANDLE;
    result = vkAllocateMemory(logicalDevice, &allocInfoOut, NULL, &outMemory);
    ERROR_CHECK(result, "vkAllocateMemory failed");

    result = vkBindBufferMemory(logicalDevice, outBuffer, outMemory, 0);
    ERROR_CHECK(result, "vkBindBufferMemory failed");

    float *outBufferMapped = NULL;
    result = vkMapMemory(logicalDevice, outMemory, 0, bufferCreateInfo.size, 0, (void **)&outBufferMapped);
    ERROR_CHECK(result, "vkMapMemory failed");

    VkShaderModuleCreateInfo createInfo = {0};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = sizeof(shader_compute_spv) / sizeof(shader_compute_spv[0]);
    createInfo.pCode = (uint32_t *)shader_compute_spv;

    VkShaderModule shaderComputeModule = VK_NULL_HANDLE;
    result = vkCreateShaderModule(logicalDevice, &createInfo, NULL, &shaderComputeModule);
    ERROR_CHECK(result, "vkCreateShaderModule failed");

    VkDescriptorSetLayoutBinding textureImageLayoutBinding = {0};
    textureImageLayoutBinding.binding = 0;
    textureImageLayoutBinding.descriptorCount = 1;
    textureImageLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    textureImageLayoutBinding.pImmutableSamplers = NULL;
    textureImageLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

    VkDescriptorSetLayoutBinding outLayoutBinding = {0};
    outLayoutBinding.binding = 1;
    outLayoutBinding.descriptorCount = 1;
    outLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    outLayoutBinding.pImmutableSamplers = NULL;
    outLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

    VkDescriptorSetLayoutBinding bindings[2] = {textureImageLayoutBinding, outLayoutBinding};

    VkDescriptorSetLayoutCreateInfo layoutInfo = {0};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
    layoutInfo.pBindings = bindings;

    VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
    result = vkCreateDescriptorSetLayout(logicalDevice, &layoutInfo, NULL, &descriptorSetLayout);
    ERROR_CHECK(result, "descriptorSetLayout failed");

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {0};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;

    VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
    result = vkCreatePipelineLayout(logicalDevice, &pipelineLayoutInfo, NULL, &pipelineLayout);
    ERROR_CHECK(result, "vkCreatePipelineLayout failed");

    VkPipelineShaderStageCreateInfo computeShaderStageInfo = {0};
    computeShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    computeShaderStageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
    computeShaderStageInfo.module = shaderComputeModule;
    computeShaderStageInfo.pName = "main";

    VkComputePipelineCreateInfo computePipelineCreateInfo = {0};
    computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    computePipelineCreateInfo.layout = pipelineLayout;
    computePipelineCreateInfo.stage = computeShaderStageInfo;

    VkPipeline computePipeline = VK_NULL_HANDLE;
    result =
        vkCreateComputePipelines(logicalDevice, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, NULL, &computePipeline);
    ERROR_CHECK(result, "vkCreateComputePipelines failed");

    VkDescriptorPoolSize poolSizes[2] = {0};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[0].descriptorCount = 1;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    poolSizes[1].descriptorCount = 1;

    VkDescriptorPoolCreateInfo poolInfo = {0};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = sizeof(poolSizes) / sizeof(poolSizes[0]);
    poolInfo.pPoolSizes = poolSizes;
    poolInfo.maxSets = 1;

    VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
    result = vkCreateDescriptorPool(logicalDevice, &poolInfo, NULL, &descriptorPool);
    ERROR_CHECK(result, "vkCreateDescriptorPool failed");

    VkDescriptorSetAllocateInfo descriptorSetAllocInfo = {0};
    descriptorSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocInfo.descriptorPool = descriptorPool;
    descriptorSetAllocInfo.descriptorSetCount = 1;
    descriptorSetAllocInfo.pSetLayouts = &descriptorSetLayout;

    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    result = vkAllocateDescriptorSets(logicalDevice, &descriptorSetAllocInfo, &descriptorSet);
    ERROR_CHECK(result, "vkAllocateDescriptorSets failed");

    VkDescriptorImageInfo textureImageInfo = {0};
    textureImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    textureImageInfo.imageView = textureImageView;
    textureImageInfo.sampler = textureSampler;

    VkDescriptorBufferInfo outBufferInfo = {0};
    outBufferInfo.buffer = outBuffer;
    outBufferInfo.offset = 0;
    outBufferInfo.range = bufferCreateInfo.size;

    VkWriteDescriptorSet descriptorWrites[2] = {0};
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = descriptorSet;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pImageInfo = &textureImageInfo;

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = descriptorSet;
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pBufferInfo = &outBufferInfo;

    vkUpdateDescriptorSets(logicalDevice, sizeof(descriptorWrites) / sizeof(descriptorWrites[0]), descriptorWrites, 0,
                           NULL);

    VkCommandBufferAllocateInfo commandBufferAllocInfo = {0};
    commandBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    commandBufferAllocInfo.commandPool = commandPool;
    commandBufferAllocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
    result = vkAllocateCommandBuffers(logicalDevice, &commandBufferAllocInfo, &commandBuffer);
    ERROR_CHECK(result, "vkAllocateCommandBuffers failed");

    VkCommandBufferBeginInfo commandBufferBeginInfo = {0};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
    ERROR_CHECK(result, "vkBeginCommandBuffer failed");

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);

    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSet, 0,
                            NULL);

    vkCmdDispatch(commandBuffer, 1, 1, 1);

    result = vkEndCommandBuffer(commandBuffer);
    ERROR_CHECK(result, "vkEndCommandBuffer failed");

    VkFenceCreateInfo fenceInfo = {0};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence fence = VK_NULL_HANDLE;
    result = vkCreateFence(logicalDevice, &fenceInfo, NULL, &fence);
    ERROR_CHECK(result, "vkCreateFence failed");

    result = vkResetFences(logicalDevice, 1, &fence);
    ERROR_CHECK(result, "vkResetFences failed");

    VkSubmitInfo submitInfo = {0};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    printf("Submitted to GPU VK_FORMAT_R32_SFLOAT 1x1 texture with value: 0x%" PRIx32 " (binary)\n",
           *((uint32_t *)&g_float_bad_value));

    result = vkQueueSubmit(computeQueue, 1, &submitInfo, fence);
    ERROR_CHECK(result, "vkQueueSubmit failed");

    result = vkWaitForFences(logicalDevice, 1, &fence, VK_TRUE, UINT64_MAX);
    ERROR_CHECK(result, "vkQueueWaitIdle failed");

    result = vkQueueWaitIdle(computeQueue);
    ERROR_CHECK(result, "vkQueueWaitIdle failed");

    printf("Retrieved sampled texture value: 0x%" PRIx32 " (binary)\n", *((uint32_t *)outBufferMapped));

    // cleanup
    if (fence)
        vkDestroyFence(logicalDevice, fence, NULL);

    if (commandBuffer)
        vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);

    if (commandPool)
        vkDestroyCommandPool(logicalDevice, commandPool, NULL);

    if (descriptorPool)
        vkDestroyDescriptorPool(logicalDevice, descriptorPool, NULL);

    if (computePipeline)
        vkDestroyPipeline(logicalDevice, computePipeline, NULL);

    if (pipelineLayout)
        vkDestroyPipelineLayout(logicalDevice, pipelineLayout, NULL);

    if (descriptorSetLayout)
        vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, NULL);

    if (shaderComputeModule)
        vkDestroyShaderModule(logicalDevice, shaderComputeModule, NULL);

    if (outMemory)
        vkFreeMemory(logicalDevice, outMemory, NULL);

    if (textureMemory)
        vkFreeMemory(logicalDevice, textureMemory, NULL);

    if (textureSampler)
        vkDestroySampler(logicalDevice, textureSampler, NULL);

    if (textureImageView)
        vkDestroyImageView(logicalDevice, textureImageView, NULL);

    if (textureImage)
        vkDestroyImage(logicalDevice, textureImage, NULL);

    if (outBuffer)
        vkDestroyBuffer(logicalDevice, outBuffer, NULL);

    if (logicalDevice)
        vkDestroyDevice(logicalDevice, NULL);

    if (physicalDevices)
        free(physicalDevices);

    if (instance)
        vkDestroyInstance(instance, NULL);

    return 0;
}