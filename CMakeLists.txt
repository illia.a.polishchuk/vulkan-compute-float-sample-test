cmake_minimum_required(VERSION 3.10)

project(vulkan-compute-minimal-sample)

find_package(Vulkan REQUIRED)

add_executable(${PROJECT_NAME} "${PROJECT_NAME}.c")
target_link_libraries(${PROJECT_NAME} PRIVATE Vulkan::Vulkan)
