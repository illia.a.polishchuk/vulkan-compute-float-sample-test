# vulkan-compute-minimal-sample
Check conversion of NaN (0xffffffff) float value sampled inside Vulkan 2d 1x1 texture

## How to build
```
mkdir build
cd build
cmake ..
cmake --build .
```
## Test results
### Ubuntu 22.04 -- Intel(R) UHD Graphics (TGL GT1)
```
./vulkan-compute-minimal-sample 
INFO: Selected physical device: Intel(R) UHD Graphics (TGL GT1)

Submitted to GPU VK_FORMAT_R32_SFLOAT 1x1 texture with value: 0xffffffff (binary)
Retrieved sampled texture value: 0xffc00000 (binary)
```
### Ubuntu 22.04 -- Intel(R) UHD Graphics 620 (KBL GT2)
```
./vulkan-compute-minimal-sample 
INFO: Selected physical device: Intel(R) UHD Graphics 620 (KBL GT2)

Submitted to GPU VK_FORMAT_R32_SFLOAT 1x1 texture with value: 0xffffffff (binary)
Retrieved sampled texture value: 0xffc00000 (binary)
```
### Windows11 -- NVIDIA GeForce RTX 2070 SUPER
```
vulkan-compute-minimal-sample.exe
INFO: Selected physical device: NVIDIA GeForce RTX 2070 SUPER

Submitted to GPU VK_FORMAT_R32_SFLOAT 1x1 texture with value: 0xffffffff (binary)
Retrieved sampled texture value: 0x7fffffff (binary)
```
### MacOS -- Apple M1
```
./vulkan-compute-minimal-sample  
INFO: Selected physical device: Apple M1

Submitted to GPU VK_FORMAT_R32_SFLOAT 1x1 texture with value: 0xffffffff (binary)
Retrieved sampled texture value: 0x7fc00000 (binary)
```
